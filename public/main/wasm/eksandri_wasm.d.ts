/* tslint:disable */
/* eslint-disable */
/**
*/
export class EksandriWasm {
  free(): void;
/**
* @returns {Promise<EksandriWasm>}
*/
  static new(): Promise<EksandriWasm>;
/**
* @param {Uint8Array} book
* @returns {any}
*/
  read_book(book: Uint8Array): any;
/**
* @param {Uint8Array} book
* @returns {Promise<any>}
*/
  create_book_from_file(book: Uint8Array): Promise<any>;
/**
* @returns {Promise<any>}
*/
  get_all_book(): Promise<any>;
/**
* @param {string} id
* @returns {Promise<any>}
*/
  delete_book(id: string): Promise<any>;
/**
* @param {any} id_list
* @returns {Promise<any>}
*/
  delete_many_book(id_list: any): Promise<any>;
/**
* @param {string} id
* @returns {Promise<any>}
*/
  get_book_by_id(id: string): Promise<any>;
/**
* @param {number} book_id
* @returns {Promise<any>}
*/
  get_book_progress(book_id: number): Promise<any>;
/**
* @param {any} book_progress
* @returns {Promise<any>}
*/
  set_book_progress(book_progress: any): Promise<any>;
/**
* @param {string} book_id
* @param {string} resource_id
* @returns {Promise<any>}
*/
  get_book_resource(book_id: string, resource_id: string): Promise<any>;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_eksandriwasm_free: (a: number) => void;
  readonly eksandriwasm_new: () => number;
  readonly eksandriwasm_read_book: (a: number, b: number, c: number) => number;
  readonly eksandriwasm_create_book_from_file: (a: number, b: number, c: number) => number;
  readonly eksandriwasm_get_all_book: (a: number) => number;
  readonly eksandriwasm_delete_book: (a: number, b: number, c: number) => number;
  readonly eksandriwasm_delete_many_book: (a: number, b: number) => number;
  readonly eksandriwasm_get_book_by_id: (a: number, b: number, c: number) => number;
  readonly eksandriwasm_get_book_progress: (a: number, b: number) => number;
  readonly eksandriwasm_set_book_progress: (a: number, b: number) => number;
  readonly eksandriwasm_get_book_resource: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
  readonly __wbindgen_export_2: WebAssembly.Table;
  readonly _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__hef2e899e3cd97d2c: (a: number, b: number, c: number) => void;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__h16ff3c72e197d7c2: (a: number, b: number, c: number, d: number) => void;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_exn_store: (a: number) => void;
  readonly wasm_bindgen__convert__closures__invoke2_mut__h72f220563ebbd8fd: (a: number, b: number, c: number, d: number) => void;
}

export type SyncInitInput = BufferSource | WebAssembly.Module;
/**
* Instantiates the given `module`, which can either be bytes or
* a precompiled `WebAssembly.Module`.
*
* @param {SyncInitInput} module
*
* @returns {InitOutput}
*/
export function initSync(module: SyncInitInput): InitOutput;

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
