/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_eksandriwasm_free(a: number): void;
export function eksandriwasm_new(): number;
export function eksandriwasm_read_book(a: number, b: number, c: number): number;
export function eksandriwasm_create_book_from_file(a: number, b: number, c: number): number;
export function eksandriwasm_get_all_book(a: number): number;
export function eksandriwasm_delete_book(a: number, b: number, c: number): number;
export function eksandriwasm_delete_many_book(a: number, b: number): number;
export function eksandriwasm_get_book_by_id(a: number, b: number, c: number): number;
export function eksandriwasm_get_book_progress(a: number, b: number): number;
export function eksandriwasm_set_book_progress(a: number, b: number): number;
export function eksandriwasm_get_book_resource(a: number, b: number, c: number, d: number, e: number): number;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
export const __wbindgen_export_2: WebAssembly.Table;
export function _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__hef2e899e3cd97d2c(a: number, b: number, c: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function _dyn_core__ops__function__FnMut__A____Output___R_as_wasm_bindgen__closure__WasmClosure___describe__invoke__h16ff3c72e197d7c2(a: number, b: number, c: number, d: number): void;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_exn_store(a: number): void;
export function wasm_bindgen__convert__closures__invoke2_mut__h72f220563ebbd8fd(a: number, b: number, c: number, d: number): void;
